/** TESTS for 600.226 Fall 2015 Project 4
 */


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Iterator;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;
import java.util.Collection;

public class MaxHeapTest {

    static MaxHeap<Integer> mh1; 
    static MaxHeap<String> mhs;
    
    static Integer[] iray = {9, 3, 2, 7, 4, 1, 10, 8, 5, 6};
    static Integer[] iray2 = {3, 1, 4, 4, 9, 5, 9, 5, 9, 1, 2, 3};
    static String[] sray = {"d", "e", "g", "h", "k", "b", "c", "f", "i", "h"};


    @Before        
    public void setup() {
        mh1 = new MaxHeap<Integer>();   //empty heap, init size 10
        mhs = new MaxHeap<String>();
        
    }

    @Test
    public void testEmptyHeap() {
        //for mh1
        assertNull(mh1.max());
        assertNull(mh1.removeMax());
        
        //for mhs
        assertNull(mhs.max());
        assertNull(mhs.removeMax());
        
        
    }

    public void testEmptyMetrics() { 
        assertTrue(mh1.isEmpty());
        assertTrue(mh1.getSize()==1);
        assertNull(mh1.max());
        assertNull(mh1.values());
        // may need fudge factor for floats
        assertTrue(mhs.isEmpty());
        assertTrue(mhs.getSize()==1);
        assertNull(mhs.max());
        assertNull(mhs.values());  
    }

    @Test
    public void testInsert() {
        assertTrue(mh1.isEmpty());
        assertEquals(mh1.getSize(), 0);
        Integer max = 0;
        for (int i=0; i < iray.length; i++) { //initial insert: 11 integers
            if (max < iray[i]) {
                max = iray[i];
            }
            mh1.insert(iray[i]);
            assertEquals(mh1.max(), max);
            assertEquals(mh1.getSize(), i + 1);
        }
        assertFalse(mh1.isEmpty());
        assertTrue(mh1.max() == 10);
        assertEquals(mh1.getSize(), 10);


        assertTrue(mhs.isEmpty());
        String maxS = " ";
        for (int i=0; i < sray.length; i++) { //initial insert: 10 Strings
            if (maxS.compareTo(sray[i]) < 0) {
                maxS = sray[i];
            }
            mhs.insert(sray[i]);
            assertTrue(mhs.max() == maxS);
        }
        assertFalse(mhs.isEmpty());
        assertTrue(mhs.max() == "k");
        assertEquals(mhs.getSize(), 10);


        //test for duplicates
        for (int i=0; i < iray2.length; i++) { //array of duplicates
            if (max < iray2[i]) {
                max = iray2[i];
            }
            mh1.insert(iray2[i]);
            assertEquals(mh1.max(), max);
        }
        assertEquals(mh1.getSize(), 22);
    }

    @Test
    public void testRemoveMax() {
        assertNull(mh1.removeMax());
        assertEquals(mh1.getSize(), 0);
        Integer max = 0;
        for (int i=0; i < iray.length/2; i++) { //initial insert into array: 5 integers
            if (max < iray[i]) {
                max = iray[i];
            }
            mh1.insert(iray[i]);
            assertEquals(mh1.max(), max);
        }
        assertEquals(mh1.getSize(), 5);
        for (int i=0; i < iray.length/2; i++) { //remove max from array
            assertEquals(mh1.removeMax(), max);
            assertEquals(mh1.getSize(), iray.length/2 - i - 1);
            max = mh1.max();
        }
        assertEquals(mh1.getSize(), 0);
        assertNull(mh1.removeMax()); 


        //test with duplicates
        /*for (int i=0; i < iray2.length; i++) { //insert into array of duplicates
            if (max < iray2[i]) {
                max = iray2[i];
            }
            mh1.insert(iray2[i]);
            assertEquals(mh1.max(), max);
        }
        assertEquals(mh1.getSize(), 12);
        for (int i=0; i < iray2.length; i++) { //remove max from array of duplicates
            assertEquals(mh1.getSize(), iray2.length - i);
            assertEquals(mh1.removeMax(), max);
            max = mh1.max();
        }
        assertNull(mh1.removeMax());
        assertEquals(mh1.getSize(), 0);*/
    }

    @Test
    public void testMax() {
        //System.out.println("TEST");
        assertNull(mh1.max());
        Integer max = 0;
        for (int i=0; i < iray.length; i++) { //initial insert: 10 integers
            if (max < iray[i]) {
                max = iray[i];
            }
            mh1.insert(iray[i]);
            assertEquals(mh1.max(), max);
        }
        for (int i=0; i < iray.length; i++) { //remove max from array
            
            assertEquals(mh1.removeMax(), max);
            max = mh1.max();
        }
        assertTrue(mh1.isEmpty());
        
        max = 0;
        for (int i=0; i < iray2.length; i++) { //array with duplicates
            if (max < iray2[i]) {
                max = iray2[i];
            }
            mh1.insert(iray2[i]);
            assertEquals(mh1.max(), max);
        }
        for (int i=0; i < iray2.length; i++) { //remove from array of duplicates
            assertEquals(mh1.removeMax(), max);
            max = mh1.max();
        }
        assertTrue(mh1.isEmpty());


        String maxS = " ";
        for (int i=0; i < sray.length; i++) { //initial insert: 10 integers
            if (maxS.compareTo(sray[i]) < 0) {
                maxS = sray[i];
            }
            mhs.insert(sray[i]);
            assertEquals(mhs.max(), maxS);
        }
        for (int i=0; i < sray.length; i++) { //remove max from array
            assertEquals(mhs.removeMax(), maxS);
            maxS = mhs.max();
        } 
        assertTrue(mhs.isEmpty());
    }

    @Test
    public void testValues() {

    }
}