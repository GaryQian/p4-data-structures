/**
 * Class containing the information of a Command.
 */
public class CommandInfo {
    
    /**
     * True for successful command, false otherwise.
     */
    public boolean success;
    
    /**
     * Address of a block.
     */
    public int address;
    
    /**
     * ID of a block.
     */
    public int id;
    
    /**
     * Size of a block.
     */
    public int size;
    
    /**
     * True if defragged, false otherwise.
     */
    public boolean defragged;

    /**
     * A Block object.
     */
    public Block block;
    
    /**
     * Constructor for Command Info (no args).
     */
    public CommandInfo() {
        this.success = false;
    }
    
    /**
     * Constructor for Command Info.
     * @param suc if successful or not
     * @param ident block id
     * @param ad block address 
     * @param s block size
     * @param df if defragged
     */
    public CommandInfo(boolean suc, int ident, int ad, int s, boolean df) {
        this.success = suc;
        this.id = ident;
        if (suc) {
            this.address = ad;
        } else {
            this.address = -1;
        }
        this.size = s;
        this.defragged = df;
    }
    
    /**
     * Constructor for Command Info.
     * @param succ if successful or not
     * @param b what block gets if was successful
     * @param ident the block id
     */
    public CommandInfo(boolean succ, Block b, int ident) {
        this.success = succ;
        if (succ) {
            this.block = b;
        } else {
            this.block = null;
        }
        this.id = ident;
    }
}