
/**
 * Worst fit.
 */
public class WorstFit extends BaseManager {
    /**
     * The free Vlocks.
     */
    MaxHeap<Block> freeBlocks;
    
    /**
     * The size.
     */
    int size;
    
    /**
     * Constructor.
     * @param size the start size
     */
    public WorstFit(int size) {
        super();
        this.freeBlocks = new MaxHeap<Block>();
        this.freeBlocks.insert(new Block(0, size, 0, true));
        this.size = size;
    }
    
    /**
     * Allocates the memory.
     * @param size the allocation size
     * @return the info.
     */
    public CommandInfo allocate(int size) {
        //System.out.println("");
        //System.out.println(this.IDCounter);
        if (size > this.size) {
            return new CommandInfo(false, this.IDCounter++, -1, size, false);
        }
        if (freeBlocks.isEmpty()) {
            this.failCount++;
            this.failSizeTotal += size;
            this.commandCount++;
            return new CommandInfo(false, this.IDCounter++, -1, size, false);
        }
        
        Block b = freeBlocks.max();
        //System.out.println(b);
        if (b == null || b.getSize() < size) {
            if (!this.defragment()) {
                this.failCount++;
                this.failSizeTotal += size;
                this.commandCount++;
                return new CommandInfo(false, this.IDCounter++, -1, size, true);
            }
            CommandInfo info = allocate(size);
            info.defragged = true;
            return info;
        }
        b = this.freeBlocks.removeMax();
        Block free = b.allocate(size, this.IDCounter);
        if (free != null) {
            this.freeBlocks.insert(free);
        }
        this.allocatedBlocks.put(b.getID(), b);
        this.commandCount++;
        return new CommandInfo(true, this.IDCounter++, b.getStartAddress(), b.getSize(), false);
    }
    
    /**
     * Deallocates a block.
     * @param id the block id
     * @return CommandInfo object containing the 
     * information after deallocating.
     */
    public CommandInfo deallocate(int id) {
        Block b = allocatedBlocks.remove(id);
        
        if (b == null) {
            return new CommandInfo(false, null, id);
        }
        b.setFree(true);
        //System.out.println("DEALLOCATING " + b);
        this.freeBlocks.insert(new Block(b.getStartAddress(), b.getSize(), 0, true));
        //System.out.println(freeBlocks.contains(new Block(0, 500, 0, true)));
        return new CommandInfo(true, b, id);
    }
    
    /**
     * Defrags the free memory.
     * @return true if defragged, false otherwise.
     */
    public boolean defragment() {
        if (freeBlocks.isEmpty()) {
            return false;
        }
        long st = this.startTime();
        Object[] temp = Sorter.bucketSort(freeBlocks.values());
        long nd = this.endTime();
        this.totalBucketsortTime = this.totalBucketsortTime + (st - nd);
        this.numThings = this.numThings + temp.length;
        
        int i = 0;
        int length = temp.length;
        boolean modified = false;
        while (i < length - 1) {
            if (temp[i] != null && temp[i + 1] != null && ((Block) temp[i]).getStartAddress() + ((Block) temp[i]).getSize() == ((Block) temp[i + 1]).getStartAddress()) {
                ((Block) temp[i]).setSize(((Block) temp[i]).getSize() + ((Block) temp[i + 1]).getSize());
                for (int j = i + 1; j < length - 1; j++) {
                    temp[j] = temp[j + 1];
                }
                temp[length - 1] = null;
                length--;
                modified = true;
            } else {
                i++;
            }
        }
        freeBlocks = new MaxHeap<Block>();
        for (Object b : temp) {
            freeBlocks.insert((Block) b);
        }
        if (modified) {
            this.defragCount++;
        }
        return modified;
    }
    
}