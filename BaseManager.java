import java.util.HashMap;

/**
 *Base Manager class. Implements Memory Manager.
 */
public class BaseManager implements MemoryManager {
    
    /**
     * Factor to mulitply milliseconds with.
     */
    protected final int factor = 10;
    
    /**
     * Hashmap with Integer keys and allocated Block values.
     */
    protected HashMap<Integer, Block> allocatedBlocks;

    /**
     * Number of defrags.
     */
    protected int defragCount;
    
    /**
     * Number of failed allocations.
     */
    protected int failCount;
    
    /**
     * Total number of fails.
     */
    protected int failSizeTotal;

    /**
     * Total amount of time.
     */
    protected double timeTotal;
    
    /**
     * Command count.
     */
    protected int commandCount;

    /**
     * Total allocation time.
     */
    protected long totalAlloTime;
    
    /**
     * Total time for quick sort.
     */
    protected long totalQuicksortTime;
    
    /**
     * Total time for bucket sort.
     */
    protected long totalBucketsortTime;

    /**
     * Number of things.
     */
    protected long numThings;

    //the free block datastructure

    /**
     * Number of Id's.
     */
    protected int idCounter;

    /**
     * Constructor for Base Manager.
     */
    public BaseManager() {
        this.idCounter = 1;
        this.allocatedBlocks = new HashMap<Integer, Block>();
        this.defragCount = 0;
        this.failCount = 0;
        this.failSizeTotal = 0;
        this.timeTotal = 0;
        this.commandCount = 0;
    }
    
    /**
     * Dummy method for allocate.
     * @param size the allocation size
     * @return null
     */
    public CommandInfo allocate(int size) {
        return null;
    }
    
    /**
     * Dummy method for deallocate.
     * @param id the block id
     * @return null
     */
    public CommandInfo deallocate(int id) {
        return null;
    }
    
    /**
     * Dummy method for defragment.
     * @return false
     */
    public boolean defragment() {
        return false;
    }
    
    /**
     * Returns the total number of fails.
     * @return the fail count
     */
    public int totalFails() {
        return this.failCount;
    }
    
    /**
     * Computes and returns the average fail size.
     * @return the average fail size
     */
    public double avgFailSize() {
        return ((double) this.failSizeTotal) / this.failCount;
    }
    
    /**
     * Computes and returns the average time.
     * @return the average time
     */
    public double avgTime() {
        return ((double) this.timeTotal) / this.commandCount;
    }
    
    /**
     * Gets the defrag count.
     * @return the number of defrags
     */
    public int defragCount() {
        return this.defragCount;
    }
    
    /**
     * Computes and returns the average sort ratio.
     * @return the average sort ratio
     */
    public double avgSortRatio() {
        return (this.totalQuicksortTime 
                + this.totalBucketsortTime) / this.numThings;
    }
    
    /**
     * Gets and returns the time at the start of the program.
     * @return the start time in milliseconds.
     */
    public long startTime() {
        return System.currentTimeMillis() * this.factor;
    }
    
    /**
     * Gets and returns the time at the end of the program.
     * @return the end time in milliseconds.
     */
    public long endTime() {
        return System.currentTimeMillis() * this.factor;
    }
}