/** TESTS for 600.226 Fall 2015 Project 4
 */


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Iterator;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;
import java.util.Collection;
import java.util.Random;

public class AVLtreeTest {

	static AVLtree<Integer> avl1; 
    static AVLtree<Integer> avl2;
    
    static Integer[] iray = {9, 3, 2, 7, 4, 1, 10, 8, 5, 6};
    static Integer[] iray2 = {3, 1, 4, 4, 9, 5, 9, 5, 9, 1, 2, 3};
    static String[] sray = {"d", "e", "g", "h", "k", "b", "c", "f", "i", "h"};

	@Before
	public void setup() {
		avl1 = new AVLtree<Integer>();
		avl2 = new AVLtree<Integer>();
	}

	@Test
	public void testEmptyTree() {
		assertTrue(avl1.isEmpty());
		assertTrue(avl1.size() == 0);
		assertNull(avl1.root());

		assertTrue(avl2.isEmpty());
		assertTrue(avl2.size() == 0);
		assertNull(avl2.root());
	}    

	@Test
	public void testAdd() {
		//test null value
		Integer nullInt = null;
		assertFalse(avl1.add(nullInt));

		//array without duplicates
		for (int i = 0; i < iray.length; i++) {
			assertTrue(avl1.add(iray[i]));
			assertTrue(avl1.contains(iray[i]));
		}
		assertEquals(avl1.size(), 10);
		assertEquals(avl1.toString(), "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]");

		//array with duplicates
		for (int i = 0; i < iray2.length; i++) {
			assertTrue(avl2.add(iray2[i]));
			assertTrue(avl2.contains(iray2[i]));
		}
		assertEquals(avl2.size(), 12);
		assertEquals(avl2.toString(), "[1, 1, 2, 3, 3, 4, 4, 5, 5, 9, 9, 9]");
	}

	@Test
	public void testRemove() {
		assertFalse(avl1.remove(5));
		assertFalse(avl2.remove(10));

		//without duplicates
		for (int i = 0; i < iray.length; i++) { //add 10 ints to array
			assertTrue(avl1.add(iray[i]));
		}
		assertEquals(avl1.size(), 10);
		for (int i = 0; i < iray.length; i++) { //delete all ints
			assertEquals(avl1.size(), iray.length - i);
			assertTrue(avl1.contains(iray[i]));
			assertTrue(avl1.remove(iray[i]));
			assertFalse(avl1.contains(iray[i]));
		}
		assertTrue(avl1.isEmpty());
		assertNull(avl1.root());

		//array with duplicates
		for (int i = 0; i < iray2.length; i++) { //add 12 ints to array
			assertTrue(avl2.add(iray2[i]));
		}
		assertEquals(avl2.size(), 12);
		for (int i = 0; i < iray2.length; i++) { //delete all ints
			assertEquals(avl2.size(), iray2.length - i);
			assertTrue(avl2.remove(iray2[i]));
		}
		assertTrue(avl2.isEmpty());
		assertNull(avl2.root());

		//Testing Re-insert: insert again into avl1, then delete again
		Random rand = new Random();
		while (avl1.size() < 50) {
			//adds a random val from iray 50 times
			assertTrue(avl1.add(iray[rand.nextInt(9 + 1)]));
		}
		int size = avl1.size();
		//deletes each value from the tree
		for (int i = 0; i < iray.length; i++) {
			while (avl1.contains(iray[i])) {
				assertTrue(avl1.remove(iray[i]));
				size--;
				assertEquals(avl1.size(), size);
			}
		}
		assertTrue(avl1.isEmpty());
		assertNull(avl1.root());
	}

}