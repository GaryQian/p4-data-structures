
/**
 * Memory Manager interface.
 *
 */
public interface MemoryManager {

    /**
     * Allocates a block.
     * @param size the size of the block to allocate.
     * @return a CommandInfo object containing the 
     * information after allocating.
     */
    CommandInfo allocate(int size);

    /**
     * Deallocates a block.
     * @param id the block id
     * @return CommandInfo object containing the 
     * information after deallocating.
     */
    CommandInfo deallocate(int id);
    
    /**
     * Whether or not was defragged.
     * @return true if defragged, false otherwise.
     */
    boolean defragment();
    
    /**
     * Gets the total number of failed allocations.
     * @return total number of fails
     */
    int totalFails();
    
    /**
     * Computes and returns the average fail size.
     * @return the average fail size.
     */
    double avgFailSize();
    
    /**
     * Computes and returns the average run time.
     * @return the average run time.
     */
    double avgTime();
    
    /**
     * Gets the number of defrags.
     * @return the number of defrags
     */
    int defragCount();
    
    /**
     * Computes and returns the average sort ratio.
     * @return the average sort ratio.
     */
    double avgSortRatio();
    
}