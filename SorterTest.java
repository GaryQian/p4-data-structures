/** TESTS for 600.226 Fall 2015 Project 4
 */


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Iterator;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;
import java.util.Collection;
import java.util.Comparator;

public class SorterTest {
    
    static Integer[] iray = {9, 3, 2, 7, 4, 1, 10, 8, 5, 6};
    static String[] sray = {"d", "e", "g", "h", "k", "b", "c", "f", "i", "h"};
	static Integer[] testiray;
	static String[] testsray;
	static Integer[] sortediray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	static String[] sortedsray = {"b", "c", "d", "e", "f", "g", "h", "h", "i", "k"};

    @Before        
    public void setup() {
		/*iray = {9, 3, 2, 7, 4, 1, 10, 7, 8, 5, 6};
		sray = {"d", "e", "g", "h", "k", "b", "c", "f", "i", "h"};*/
		
		testiray = iray.clone();
		testsray = sray.clone();
    }
	
	@Test
	public void testQuickSort() {
		testiray = (Integer[]) Sorter.quickSort(testiray, 0, 9);
		
		assertTrue(testiray.length == 10);
		for (int i = 0; i < 10; i++) {
			assertEquals(testiray[i], sortediray[i]);
		}
		testsray = (String[]) Sorter.quickSort(testsray, 0, 9);
		
		assertTrue(testsray.length == 10);
		for (int i = 0; i < 10; i++) {
			assertEquals(testsray[i], sortedsray[i]);
		} 
	}
	
	@Test
	public void testBucketSort() {
		testiray = (Integer[]) Sorter.bucketSort(testiray);
		
		assertTrue(testiray.length == 10);
		for (int i = 0; i < 10; i++) {
			assertEquals(testiray[i], sortediray[i]);
		}
		
		testsray = (String[]) Sorter.bucketSort(testsray);
		
		assertTrue(testsray.length == 10);
		for (int i = 0; i < 10; i++) {
			assertEquals(testsray[i], sortedsray[i]);
		}
	}
	
}