/** Test for WorstFit 
* @author Will Scerbo, Gary Qian, Chris Meseha
*/

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Iterator;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;
import java.util.Collection;

public class WorstFitTest {	
	
	static WorstFit worst1;
	static WorstFit worst2;
	static WorstFit worst3;
	
	@Before
	public void setup() {
		worst1 = new WorstFit(100);
		worst2 = new WorstFit(100);
		worst3 = new WorstFit(100);
	}
	
	@Test
	public void testAllocate() {
		assertTrue(worst1.size == 100);
		assertTrue(worst1.allocate(20).success == true); //1
		assertTrue(worst1.allocate(20).success == true); //2
		assertTrue(worst1.allocate(15).success == true); //3
		assertTrue(worst1.allocate(15).success == true); //4
		assertTrue(worst1.allocate(15).success == true); //5
		assertTrue(worst1.allocate(15).success == true); //6
		assertTrue(worst1.deallocate(2).success == true);
		assertTrue(worst1.deallocate(3).success == true);
		assertTrue(worst1.deallocate(5).success == true);
		assertTrue(worst1.deallocate(6).success == true);
		assertTrue(worst1.allocate(30).success == true); //7, plus defrag
		assertTrue(worst1.allocate(35).success == false); //fails due to behavior of 7
		
	}
	
	@Test
	public void testDeallocate() {
		//when deallocate succeeds
		assertTrue(worst2.size == 100);
		assertTrue(worst2.allocate(100).success == true); //1
		assertTrue(worst2.deallocate(1).success == true);
		
		//when deallocate fails
		assertTrue(worst2.allocate(20).success == true); //2
		assertTrue(worst2.allocate(20).success == true); //3
		assertTrue(worst2.allocate(20).success == true); //4
		assertTrue(worst2.allocate(20).success == true); //5
		assertTrue(worst2.allocate(20).success == true); //6
		assertTrue(worst2.deallocate(2).success == true);
		assertTrue(worst2.deallocate(2).success == false); //already tried to deallocate
		assertTrue(worst2.deallocate(3).success == true);
		assertTrue(worst2.deallocate(3).success == false); //already tried to deallocate
		assertTrue(worst2.deallocate(4).success == true);
		assertTrue(worst2.deallocate(4).success == false); //already tried to deallocate
		assertTrue(worst2.deallocate(5).success == true);
		assertTrue(worst2.deallocate(5).success == false); //already tried to deallocate
		assertTrue(worst2.deallocate(6).success == true);
		assertTrue(worst2.deallocate(6).success == false); //already tried to deallocate
		assertTrue(worst2.allocate(100).success == true); //7
		assertTrue(worst2.deallocate(7).success == true);
	}
	
	@Test
	public void testDefragment() {
		//when defrag fails
		assertTrue(worst3.size == 100);
		assertTrue(worst3.allocate(10).success == true); //1
		assertTrue(worst3.allocate(10).success == true); //2
		assertTrue(worst3.allocate(10).success == true); //3
		assertTrue(worst3.allocate(10).success == true); //4
		assertTrue(worst3.allocate(10).success == true); //5
		assertTrue(worst3.allocate(10).success == true); //6
		assertTrue(worst3.allocate(10).success == true); //7
		assertTrue(worst3.allocate(10).success == true); //8
		assertTrue(worst3.allocate(10).success == true); //9
		assertTrue(worst3.allocate(10).success == true); //10
		assertTrue(worst3.deallocate(1).success == true);
		assertTrue(worst3.deallocate(3).success == true);
		assertTrue(worst3.deallocate(5).success == true);
		assertTrue(worst3.deallocate(7).success == true);
		assertTrue(worst3.deallocate(9).success == true);
		assertTrue(worst3.allocate(50).success == false); //doesn't defrag and fails
		assertTrue(worst3.deallocate(2).success == true);
		assertTrue(worst3.deallocate(4).success == true);
		assertTrue(worst3.deallocate(6).success == true);
		assertTrue(worst3.deallocate(8).success == true);
		assertTrue(worst3.deallocate(10).success == true);
		
		//when defrag succeeds
		assertTrue(worst3.allocate(20).success == true); //11
		
		
		
	}
}