del *.class
javac AVLtree.java
javac MaxHeap.java
javac WorstFit.java
javac BestFit.java
javac FirstFit.java
javac MemSimMain.java

javac -cp .;junit-4.12.jar MaxHeapTest.java
java -cp .;junit-4.12.jar;hamcrest-core-1.3.jar org.junit.runner.JUnitCore MaxHeapTest



javac -cp .;junit-4.12.jar AVLtreeTest.java
java -cp .;junit-4.12.jar;hamcrest-core-1.3.jar org.junit.runner.JUnitCore AVLtreeTest

javac -cp .;junit-4.12.jar BestFitTest.java
java -cp .;junit-4.12.jar;hamcrest-core-1.3.jar org.junit.runner.JUnitCore BestFitTest

javac -cp .;junit-4.12.jar FirstFitTest.java
java -cp .;junit-4.12.jar;hamcrest-core-1.3.jar org.junit.runner.JUnitCore FirstFitTest

javac -cp .;junit-4.12.jar WorstFitTest.java
java -cp .;junit-4.12.jar;hamcrest-core-1.3.jar org.junit.runner.JUnitCore WorstFitTest


java MemSimMain test1.txt

javac -cp .;junit-4.12.jar SorterTest.java
java -cp .;junit-4.12.jar;hamcrest-core-1.3.jar org.junit.runner.JUnitCore SorterTest