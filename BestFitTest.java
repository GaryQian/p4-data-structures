/** test for BestFit 
* @author Will Scerbo, Gary Qian, Chris Meseha
*/

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Iterator;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;
import java.util.Collection;

public class BestFitTest {
	static BestFit best1;
	static BestFit best2;
	static BestFit best3;
	
	@Before
	public void setup() {
		best1= new BestFit(100);
		best2 = new BestFit(100);
		best3 = new BestFit(100);
	}
	
	@Test
	public void testAllocate() {
		assertTrue(best1.allocate(100).success == true); //1
		assertTrue(best1.allocate(1).success == false);
		assertTrue(best1.deallocate(1).success == true);
		assertTrue(best1.allocate(20).success == true); //2
		assertTrue(best1.allocate(20).success == true); //3
		assertTrue(best1.allocate(20).success == true); //4
		assertTrue(best1.allocate(20).success == true); //5
		assertTrue(best1.allocate(20).success == true); //6
		assertTrue(best1.deallocate(3).success == true);
		assertTrue(best1.deallocate(5).success == true);
		assertTrue(best1.allocate(40).success == false); 
		assertFalse(best1.deallocate(2).success == true);
		assertTrue(best1.deallocate(4).success == true);
		assertTrue(best1.deallocate(6).success == true); // cleared
		
		assertTrue(best1.allocate(30).success == true); //7
		assertTrue(best1.allocate(30).success == true); //8
		/*assertTrue(best1.allocate(30).success == true); //9
		assertTrue(best1.allocate(10).success == true); //10
		assertTrue(best1.deallocate(8).success == true);
		assertTrue(best1.deallocate(10).success = true);
		assertTrue(best1.allocate(10).success == true); //11
		assertTrue(best1.allocate(30).success == true); //12*/
	}
	
	@Test 
	public void testDeallocate() {
		assertTrue(best2.deallocate(1).success == false); //nonsense
		assertTrue(best2.allocate(20).success == true); //1
		assertTrue(best2.allocate(20).success == true); //2 
		assertTrue(best2.allocate(20).success == true); //3
		assertTrue(best2.allocate(20).success == true); //4
		assertTrue(best2.allocate(20).success == true); //5
		assertTrue(best2.deallocate(1).success == true);
		assertTrue(best2.deallocate(2).success == true);
		assertTrue(best2.deallocate(3).success == true);
		assertTrue(best2.deallocate(4).success == true);
		assertTrue(best2.deallocate(5).success == true); // now it's empty
		assertTrue(best2.deallocate(5).success == false); //redundant 
		
	}
	
	@Test
	public void testDefragment() {
		assertEquals(best3.size, 100);
		assertTrue(best3.allocate(20).success == true); //1
		assertTrue(best3.allocate(20).success == true); //2
		assertTrue(best3.allocate(10).success == true); //3
		assertTrue(best3.allocate(30).success == true); //4
		assertTrue(best3.allocate(20).success == true); //5
		assertTrue(best3.deallocate(3).success == true);
		assertTrue(best3.deallocate(4).success == true);
		assertTrue(best3.allocate(31).success == true); // forces defrag, fusing 10 block with 30 block
		assertTrue(best3.allocate(9).success == true);
		assertTrue(best3.allocate(1).success == false); // full
	}
	
	
}