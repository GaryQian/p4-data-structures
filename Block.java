/**
 * Block class representing block of memory.
 */
public class Block implements Comparable<Block> {

    /**
     * The start address of a block. 
     */
    private int startAdd;
    
    /**
     * The size of a block.
     */
    private int size;
    
    /**
     * A block's ID number.
     */
    private int id;
    
    /**
     * If block is free or not.
     */
    private boolean free;
    
    /**
     * Block String for each block.
     */
    private String blockString;
    
    /**
     * Constructor for block, no arguments.
     */
    public Block() {
        this.startAdd = 0;
        this.size = 0;
        this.id = 0;
        this.free = false;
    }
    
    /**
     * Constructor for block.
     * @param startAddress the block's start address.
     * @param s the blocks size
     */
    public Block(int startAddress, int s) {
        this.startAdd = startAddress;
        this.size = s;
        this.id = 0;
        this.free = false;
    }
    
    /**
     * Constructor for block.
     * @param startAddress the block's start address.
     * @param s the blocks size
     * @param identity the block id
     */
    public Block(int startAddress, int s, int identity) {
        this.startAdd = startAddress;
        this.size = s;
        this.id = identity;
        this.free = false;
    }
    
    /**
     * Constructor for block.
     * @param startAddress the block's start address.
     * @param s the blocks size
     * @param identity the block id
     * @param freeBlock if block is free or not
     */
    public Block(int startAddress, int s, int identity, boolean freeBlock) {
        this.startAdd = startAddress;
        this.size = s;
        this.id = identity;
        this.free = freeBlock;
    }
    
    /**
     * Gets the start address of the block.
     * @return the start address
     */
    public int getStartAddress() {
        return this.startAdd;
    }
    
    /**
     * Sets the start address of a block.
     * @param startAddress the new start address
     */
    public void setStartAddress(int startAddress) {
        this.startAdd = startAddress;
    }
    
    /**
     * Gets the size of the block.
     * @return the start address
     */
    public int getSize() {
        return this.size;
    }
    
    /**
     * Sets the size of a block.
     * @param s the new size.
     */
    public void setSize(int s) {
        this.size = s;
    }
    
    /**
     * Gets the ID of the block.
     * @return the start address
     */
    public int getID() {
        return this.id;
    }
    
    /**
     * Sets the ID of the block.
     * @param identity the new ID
     */
    public void setID(int identity) {
        this.id = identity;
    }
    
    /**
     * Gets whether block is free or not.
     * @return true if free block, false otherwise
     */
    public boolean getFree() {
        return this.free;
    }
    
    /**
     * Sets whether the block is free or not.
     * @param freeBlock a free block
     */
    public void setFree(boolean freeBlock) {
        this.free = freeBlock;
    }
    
    /**
     * Allocates a block.
     * @param s the size of the block.
     * @param idCounter the id of the block
     * @return the allocated block
     */
    public Block allocate(int s, int idCounter) {
        this.setFree(false);
        this.id = idCounter;
        if (s == this.size) {
            return null;
        }
        Block b = new Block(this.startAdd + s, this.size - s, 0, true);
        this.size = s;
        return  b;
    }
    
    /**
     * Creates a String including a block's data in the following format:
     * Start Address:
     * Size: 
     * ID:
     * Free Block? 
     * @return the block string
     */
    public String toString() {
        this.blockString = "";
        this.blockString = "Start Address: " + this.startAdd + " "
            + "Size: " + this.size + "\n";
        return this.blockString;
    }
    
    /**
     * Compares the sizes of two blocks.
     * @param o the other block that this block compares to.
     * @return the difference in size.
     */
    public int compareTo(Block o) {
        if (o == null) {
            return this.size;
        }
        return this.size - o.getSize();
    }
    
    /**
     * Compares the addresses of two blocks.
     * @param b1 the first block
     * @param b2 the second block
     * @return the difference in start addresses
     */
    public int compare(Block b1, Block b2) {
        return b1.getStartAddress() - b2.getStartAddress();
    }
    
    /**
     * Hashcode for block.
     * @return the hashcode (the start address).
     */
    public int hashCode() {
        return this.startAdd;
    }
    
    /**
     * Checks equality.
     * @param o the block to compare.
     * @return true if equal
     */
    public boolean equals(Object o) {
        Block b = (Block) o;
        if (b != null && b.size == this.size && b.startAdd == this.startAdd) {
            return true;
        }
        return false;
    }
}