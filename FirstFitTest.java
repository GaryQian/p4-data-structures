/** Test for FirstFit 
* @author Will Scerbo, Gary Qian, Chris Meseha
  */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Iterator;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;
import java.util.Collection;

public class FirstFitTest {
	static FirstFit first1;
	static FirstFit first2;
	static FirstFit first3;
	static FirstFit firstEmpty1;
	static FirstFit firstTrivial1;
	
	@Before
	public void setup() {
		first1 = new FirstFit(100);
		first2 = new FirstFit(100);
		first3 = new FirstFit(100);
		firstEmpty1 = new FirstFit(0);
		firstTrivial1 = new FirstFit(1);
	}
	@Test
	public void testAllocate() {
		assertTrue(first1.size == 100);
		assertTrue(first1.allocate(100).success == true);
		assertTrue(first1.allocate(1).success == false); //fail
		assertTrue(first1.deallocate(1).success == true);
		
		assertTrue(first1.allocate(10).success == true); //2
		assertTrue(first1.allocate(40).success == true); //3
		assertTrue(first1.allocate(30).success == true); //4
		assertTrue(first1.allocate(20).success == true); //5
		
		assertTrue(first1.deallocate(3).success == true); 
		assertTrue(first1.deallocate(5).success == true);
		assertTrue(first1.allocate(20).success == true); //6
		assertTrue(first1.allocate(40).success == false); //fails
		
	}
	
	@Test
	public void testDeallocate() {
		
		assertTrue(first2.size == 100);
		assertTrue(first2.deallocate(100).success == false); //nonsensical entry
		assertTrue(first2.allocate(20).success == true); //1
		assertTrue(first2.allocate(20).success == true); //2
		assertTrue(first2.deallocate(1).success == true);
		assertTrue(first2.deallocate(1).success == false); //already been deallocated
		
		assertTrue(first2.allocate(20).success == true); //3
		
	}
	@Test 
	public void testDefragment() {
		assertTrue(first3.size == 100);
		assertTrue(first3.allocate(20).success == true); //1
		assertTrue(first3.allocate(20).success == true); //2
		assertTrue(first3.allocate(60).success == true); //3
		assertTrue(first3.deallocate(1).success == true);
		assertTrue(first3.deallocate(3).success == true);
		assertTrue(first3.allocate(80).success == false); //4, tries to defrag and fails
		assertTrue(first3.deallocate(2).success == true); //clear
		
		assertTrue(first3.allocate(10).success == true); //4
		assertTrue(first3.allocate(10).success == true); //5
		assertTrue(first3.allocate(10).success == true); //6
		assertTrue(first3.allocate(10).success == true); //7
		assertTrue(first3.allocate(10).success == true); //8
		assertTrue(first3.allocate(10).success == true); //9
		assertTrue(first3.allocate(10).success == true); //10
		assertTrue(first3.allocate(10).success == true); //11
		assertTrue(first3.allocate(10).success == true); //12
		assertTrue(first3.allocate(10).success == true); //13
		assertTrue(first3.deallocate(5).success == true);
		assertTrue(first3.deallocate(7).success == true);
		assertTrue(first3.deallocate(9).success == true);
		assertTrue(first3.deallocate(11).success == true);
		assertTrue(first3.deallocate(13).success == true);
		assertTrue(first3.allocate(50).success == false); //tries to defrag and fails
		
	}
}