
import java.util.ArrayList;

/**
 * Utility class that contains two sorting algorithms.
 * @author Will Scerbo, Chris Meseha, Gary Qian
 */
public final class Sorter {
    /**
     * Empty constructor. prevents Instantiation. 
     */
    private Sorter() {
    }

    /**
     * Quick sort using median pivots - recursive.
     * @param b the array to sort.
     * @param lowerBound the current lower bound to check at.
     * @param upperBound the current upper bound to check at.
     * @return the sorted array
     */
    public static Object[] quickSort(Object[] b, 
            int lowerBound, int upperBound) {

        /** 
         * sorting bounds
         */
        if (upperBound - lowerBound < 2 || upperBound > b.length || lowerBound < 0) {
            return b;
        }
        int iLow = lowerBound;
        int iHigh = upperBound;
        Object p1 = b[lowerBound];
        Object p2 = b[upperBound / 2];
        Object p3 = b[upperBound];
        int pivot = 0;
        if (p1.hashCode() > p2.hashCode()) {
            if (p2.hashCode() > p3.hashCode()) {
                pivot = upperBound / 2;
            } else if (p1.hashCode() > p3.hashCode()) {
                pivot = upperBound;
            } else {
                pivot = lowerBound;
            }
        } else {
            if (p1.hashCode() > p3.hashCode()) {
                pivot = lowerBound;
            } else if (p2.hashCode() > p3.hashCode()) {
                pivot = upperBound;
            } else {
                pivot = upperBound / 2;
            }
        }
        Object temp = b[upperBound];
        b[upperBound] = b[pivot];
        b[pivot] = temp;

        while (iLow != iHigh) {
            while (b[iLow].hashCode() < b[upperBound].hashCode()) {
                iLow++;
            }
            while (b[iHigh].hashCode() > b[upperBound].hashCode()) {
                iHigh--;
            }
            Object temp2 = b[iLow];
            b[iLow] = b[iHigh];
            b[iHigh] = temp2;
        }
        Object temp3 = b[upperBound];
        b[upperBound] = b[iHigh];
        b[iHigh] = temp3;

        quickSort(b, lowerBound, iLow - 1);
        quickSort(b, iHigh + 1, upperBound);
        return b;
    }

    /**
     * Bucket sort.
     * @param b the array to sort
     * @return the sorted array
     */
    public static Object[] bucketSort(Object[] b) {
        if (b[0] == null) {
            return b;
        }
        int max = 0;
        for (int i = 0; i < b.length; i++) {
            if (b[i] != null && b[i].hashCode() > max) {
                max = b[i].hashCode();
            }
        }
        ArrayList<ArrayList<Object>> buckets 
            = new ArrayList<ArrayList<Object>>();
        for (int i = 0; i <= max; i++) {
            buckets.add(new ArrayList<Object>());
        }
        for (int i = 0; i < b.length; i++) {
            if (b[i] != null) {
                buckets.get(b[i].hashCode()).add(b[i]);
            }
        }
        int count = 0;
        for (ArrayList<Object> o : buckets) {
            for (Object o2 : o) {
                b[count] = o2;
                count++;
            }
        }
        return b;
    }
}
