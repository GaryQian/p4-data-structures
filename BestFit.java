
/**
 * The best fit implemenation.
 */
public class BestFit extends BaseManager {
    
    /**
     * The free Vlocks.
     */
    AVLtree<Block> freeBlocks;
    
    /**
     * The size.
     */
    int size;
    
    /**
     * Constructor.
     * @param size the start size
     */
    public BestFit(int size) {
        super();
        this.freeBlocks = new AVLtree<Block>();
        this.freeBlocks.add(new Block(0, size, 0, true));
        this.size = size;
    }
    
    /**
     * Allocates the memory.
     * @param size the allocation size
     * @return the info.
     */
    public CommandInfo allocate(int size) {
        if (size > this.size) {
            return new CommandInfo(false, this.IDCounter++, -1, size, false);
        }
        if (freeBlocks.isEmpty()) {
            this.failCount++;
            this.failSizeTotal += size;
            this.commandCount++;
            return new CommandInfo(false, this.IDCounter++, -1, size, false);
        }
        Block b = null;
        for (Block block : freeBlocks.inOrder()) {
            if (block.getSize() >= size) {
                b = block;
                break;
            }
        }
        if (b == null) {
            if (!this.defragment()) {
                this.failCount++;
                this.failSizeTotal += size;
                this.commandCount++;
                return new CommandInfo(false, this.IDCounter++, -1, size, true);
            }
            CommandInfo info = allocate(size);
            info.defragged = true;
            return info;
        }
        freeBlocks.remove(b);
        this.freeBlocks.add(b.allocate(size, this.IDCounter));
        this.allocatedBlocks.put(b.getID(), b);
        this.commandCount++;
        return new CommandInfo(true, this.IDCounter++, b.getStartAddress(), b.getSize(), false);
    }
    
    /**
     * Deallocates a block.
     * @param id the block id
     * @return CommandInfo object containing the 
     * information after deallocating.
     */
    public CommandInfo deallocate(int id) {
        Block b = allocatedBlocks.remove(id);
        if (b == null) {
            return new CommandInfo(false, null, id);
        }
        b.setFree(true);
        this.freeBlocks.add(b);
        return new CommandInfo(true, b, id);
    }
    
    /**
     * Defrags the free memory.
     * @return true if defragged, false otherwise.
     */
    public boolean defragment() {
        Block[] temp = new Block[freeBlocks.size()];
        int i = 0;
        Iterable<Block> iter = freeBlocks.inOrder();
        for (Block b : iter) {
            temp[i] = b;
            i++;
        }
        long st = startTime();
        temp = (Block[]) Sorter.bucketSort(temp);
        long nd = endTime();
        this.totalBucketsortTime = this.totalBucketsortTime + (st - nd);
        this.numThings = this.numThings + temp.length;
        i = 0;
        int length = temp.length;
        boolean modified = false;
        while (i < length - 1) {
            if (temp[i].getStartAddress() + temp[i].getSize() == temp[i + 1].getStartAddress()) {
                temp[i].setSize(temp[i].getSize() + temp[i + 1].getSize());
                for (int j = i + 1; j < length - 1; j++) {
                    temp[j] = temp[j + 1];
                }
                temp[length - 1] = null;
                length--;
                modified = true;
            } else {
                i++;
            }
        }
        freeBlocks = new AVLtree<Block>();
        for (Block b : temp) {
            freeBlocks.add(b);
        }
        if (modified) {
            this.defragCount++;
        }
        return modified;
    }
    
}