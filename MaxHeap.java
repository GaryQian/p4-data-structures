/**
 * PriorityQue Max Heap implementation.
 * @author Will Scerbo, Gary Qian, Chris Meseha
 * @param <V> Comparable generic value 
 */
public class MaxHeap<V extends Comparable<? super V>> {
    
    /**
     * array of objects.
     */
    V[] vArray;
    
    /**
     * size of the array initially.
     */
    private final int initSize = 10;
    
    /**
     * size of the array.
     */
    private int size;
    
    /**
     * maximum size of the array.
     */
    private int maxSize;

     

    /**
     * constructor.
    */
    public MaxHeap() {
        this.vArray = (V[]) new Comparable[this.initSize];
        this.size = 1;
        this.maxSize = this.initSize;
    }
    /**
     * inserts an element of type V in the PQ.
     * @param entry the entry inserted
     */
    public void insert(V entry) {
        
        if (this.size + 1 == this.maxSize) {
            this.maxSize *= 2;
            V[] tempArray = (V[]) new Comparable[this.maxSize + 1];
            for (int i = 0; i < this.maxSize / 2; i++) {
                tempArray[i] = this.vArray[i];
            }
            this.vArray = tempArray;
        }
        this.vArray[this.size] = entry;
        
        //bubble up
        
        //this.bubbleUp(this.size);
        int count = this.size / 2;
        int count2 = this.size;
        //System.out.println("size" + this.size);
        while (this.vArray[count] != null 
                && this.vArray[count].compareTo(entry) < 0) {
            //System.out.println("Bubbling up");
            V temp = this.vArray[count];
            this.vArray[count] = this.vArray[count2];
            this.vArray[count2] = temp;
            count2 = count;
            count /= 2;
        }
        //System.out.println("At pos 1: " + vArray[size]);
        /*for (int i = 1; i <= size; i++) {
            //System.out.print(vArray[i] + " ");
        }*/
        this.size++;
    }
    
    /**
     * removes the maximum element from the PQ.
     * @return what you removed
     */
    public V removeMax() {
        if (this.size == 1) {
            return null;
        }
        V temp = this.vArray[1];
        this.vArray[1] = this.vArray[this.size - 1];
        this.vArray[this.size - 1] = null;
        
        int count = 1;
        int count2 = count * 2;
        int count3 = (count * 2) + 1;
        if (count2 > this.maxSize - 1) {
            count2 = 0;
        }
        if (count3 > this.maxSize - 1) {
            count3 = 0;
        }
        
        this.bubble(count, count2, count3);
        this.size--;
        return temp;
    }
    
    /**Bubbles down after removing the max element from the PQ.
     * @param count the position in vArray
     * @param count2 position in vArray
     * @param count3 position in vArray
     */
    private void bubble(int count, int count2, int count3) {
        while ((this.vArray[count2] != null 
                && this.vArray[count2].compareTo(this.vArray[count]) > 0) 
                || (this.vArray[count3] != null 
                && this.vArray[count3].compareTo(this.vArray[count]) > 0)) {
            if (this.vArray[count3] == null || (this.vArray[count2] != null 
                    && this.vArray[count2].compareTo(this.vArray[count]) > 0)) {
                V swap = this.vArray[count2];
                this.vArray[count2] = this.vArray[count];
                this.vArray[count] = swap;
                count = count2;
            } else {
                V swamp = this.vArray[count3];
                this.vArray[count3] = this.vArray[count];
                this.vArray[count] = swamp;
                count = count3;
            }
            count2 = count * 2;
            count3 = (count * 2) + 1;
            if (count2 > this.maxSize - 1) {
                count2 = 0;
            }
            if (count3 > this.maxSize - 1) {
                count3 = 0;
            }
        }
    }
    
    /**
     * finds and returns the maximum priority element.
     * @return Node
     */
    public V max() {
        V max = this.vArray[1];
        /*for (int i = 0; i < this.maxSize; i++) {
            if (max.compareTo(this.vArray[i]) < 0) {
                max = this.vArray[i];
            }
        }*/
        return this.vArray[1];
    }

    /**
     * Checks if Max Heap is Empty. 
     * @return true if heap is empty, false otherwise.
     */
    public boolean isEmpty() {
        return this.size <= 1;
    }
    
    /**
     * Gets the size of the heap.
     * @return the size of the heap.
     */
    public int getSize() {
        return this.size - 1;
    }
    
    /**
     * Clears out the heap.
     */
    public void clear() {
        this.vArray = (V[]) new Comparable[this.initSize];
        this.size = 1;
        this.maxSize = this.initSize;
    }
    
    /**
     * Checks if the val is in the heap.
     * @param val the val to search for.
     * @return true if found.
     */
    public boolean contains(V val) {
        for (int i = 0; i < this.vArray.length; i++) {
            if (val.equals(this.vArray[i])) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Returns the current values in the heap.
     * @return the current max heap array
     */
    public V[] values() {
        V[] temp = (V[]) new Comparable[this.size - 1];
        for (int i = 0; i < this.size - 1; i++) {
            temp[i] = (V) this.vArray[i + 1];
        }
        return temp;
    }
}